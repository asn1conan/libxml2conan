#!/bin/sh
original_pwd=$(pwd)
current_script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $current_script_dir

prefix=$current_script_dir
libdir="$current_script_dir/lib/linux/centos7"

function build {
configure_dir=$1
compiler_flags=$2

./configure --prefix=$prefix --libdir="$libdir/$configure_dir" --without-icu --without-iconv --without-lzma --without-zlib --disable-shared $compiler_flags
make install
make clean
}

./autogen.sh
build "Release" ""
build "Debug" "--with-debug"

cd $original_pwd 




